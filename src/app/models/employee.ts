export class Employee{
    employeeID : number;
    employeeName : string;
    department: string;
    emailID : string;
    doj : Date;
}