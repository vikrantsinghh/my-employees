import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from 'src/app/models/employee';
import { Observable, Subject } from 'rxjs';
import { Department } from '../models/department';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  // public ApiUrl : string = "https://localhost:5001/api";
  public ApiUrl : string = "https://localhost:44325/api";

  formData: Employee;
  
  constructor(private http: HttpClient) { }

  getEmployees(): Observable<Employee[]>{
    return this.http.get<Employee[]>(`${this.ApiUrl}/Employees/GetEmployeeDetails`);
  }

  getDepartments(): Observable<Department[]>{
    return this.http.get<Department[]>(`${this.ApiUrl}/Department/GetDepartments`);
  }

  addEmployee(emp: Employee){
    return this.http.post(`${this.ApiUrl}/Employees/AddEmployee`, emp, {responseType: 'text'});
  }

  updateEmployee(emp: Employee){
    console.log(emp.doj);
    //emp.doj.setMinutes((emp.doj.getMinutes() + emp.doj.getTimezoneOffset()));
    //console.log(emp.doj);
    return this.http.put(`${this.ApiUrl}/Employees/EditEmployee`, emp, {responseType: 'text'});
  }

  deleteEmployee(id: number){
    return this.http.delete(`${this.ApiUrl}/Employees/DeleteEmployee?id=${id}`, {responseType: 'text'});
  }

  private _listeners = new Subject<any>();
  listen():Observable<any>{
    return this._listeners.asObservable();
  }
  
  filter(filterBy: string){
    this._listeners.next(filterBy);
  }
}
