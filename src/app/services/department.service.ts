import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Department } from 'src/app/models/department';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  // public ApiUrl : string = "https://localhost:5001/api";
  public ApiUrl : string = "https://localhost:44325/api";
  

  formData: Department;
  
  constructor(private http: HttpClient) { }

  getDepartments(): Observable<Department[]>{
    return this.http.get<Department[]>(`${this.ApiUrl}/Department/GetDepartments`);
  }

  addDepartments(dept: Department){
    return this.http.post(`${this.ApiUrl}/Department/AddDepartment`, dept, {responseType: 'text'});
  }

  updateDepartments(dept: Department){
    return this.http.put(`${this.ApiUrl}/Department/EditDepartment`, dept, {responseType: 'text'});
  }

  deleteDepartments(id: number){
    return this.http.delete(`${this.ApiUrl}/Department/DeleteDepartment?id=${id}`, {responseType: 'text'});
  }

  private _listeners = new Subject<any>();
  listen():Observable<any>{
    return this._listeners.asObservable();
  }
  
  filter(filterBy: string){
    this._listeners.next(filterBy);
  }
}
