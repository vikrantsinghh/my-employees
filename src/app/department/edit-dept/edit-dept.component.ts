import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DepartmentService } from 'src/app/services/department.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-edit-dept',
  templateUrl: './edit-dept.component.html',
  styleUrls: ['./edit-dept.component.scss']
})
export class EditDeptComponent implements OnInit {

  constructor(private dialogBox: MatDialogRef<EditDeptComponent>,
    public deptService: DepartmentService,
    private _snackBar: MatSnackBar) { }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }
  ngOnInit(): void {
  }
  onSubmit(form: NgForm){
    console.log(form.value);
    this.deptService.updateDepartments(form.value).subscribe(res =>{
      this.openSnackBar(res, null);
     //alert(res);
    });
  }

  onClose(){
    this.dialogBox.close();
    this.deptService.filter("Refresh now");
  }

}
