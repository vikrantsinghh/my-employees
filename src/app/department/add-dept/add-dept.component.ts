import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DepartmentService } from 'src/app/services/department.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-add-dept',
  templateUrl: './add-dept.component.html',
  styleUrls: ['./add-dept.component.scss']
})
export class AddDeptComponent implements OnInit {

  constructor(private dialogBox: MatDialogRef<AddDeptComponent>,
              public deptService: DepartmentService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.resetForm();
  }

  resetForm(form?: NgForm){
    if(form!=null)
      form.resetForm();

    this.deptService.formData = {
      departmentID : 0,
      departmentName : ''
    }
  }

  onClose(){
    this.dialogBox.close();
    this.deptService.filter("Refresh now");
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }

  onSubmit(form: NgForm){
    console.log(form.value);
    this.deptService.addDepartments(form.value).subscribe(res =>{
      this.resetForm(form);
      this.openSnackBar(res, null);
     //alert(res);
    });
  }

}
