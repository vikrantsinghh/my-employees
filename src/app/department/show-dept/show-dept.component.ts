import { Component, OnInit, ViewChild } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort'
import { MatInput } from '@angular/material/input'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';

import { Department } from '../../models/department'
import { AddDeptComponent } from 'src/app/department/add-dept/add-dept.component'
import { DepartmentService } from 'src/app/services/department.service';
import { EditDeptComponent } from '../edit-dept/edit-dept.component';

@Component({
  selector: 'app-show-dept',
  templateUrl: './show-dept.component.html',
  styleUrls: ['./show-dept.component.scss']
})
export class ShowDeptComponent implements OnInit {

  constructor(private departmentService : DepartmentService,
              private dialog : MatDialog,
              private _snackBar: MatSnackBar) { 
                this.departmentService.listen().subscribe((m:any)=>{
                  console.log(m);
                  this.refreshDepartment();
                });
              }

  isLoading = true;
  noData = false;
  lstDepartment : MatTableDataSource<any>;
  displayedColumns : string[] = ['Options', 'departmentID', 'departmentName'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit(): void {
    this.refreshDepartment();
    
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }
  
  refreshDepartment(){
    
    this.departmentService.getDepartments().subscribe(data => {
      this.isLoading = false;
      this.lstDepartment = new MatTableDataSource(data);
      this.lstDepartment.sort = this.sort;
      this.lstDepartment.paginator = this.paginator;
      this.lstDepartment.paginator._intl.itemsPerPageLabel = "Departments per page:";
    },
    error => {
      this.isLoading = false;
      this.lstDepartment = null;
    }
    );
  }

  applyFilter(filter: string){
    // console.log("noData: ",this.noData);
    this.lstDepartment.filter = filter.trim().toLocaleLowerCase();
    if(this.lstDepartment.filteredData.length == 0)
      this.noData = true;
    else
      this.noData = false;
  }

  onEdit(dept : Department){
    console.log(dept);
    this.departmentService.formData = dept;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "70%";
    this.dialog.open(EditDeptComponent, dialogConfig);
  }

  onDelete(Id: number){
    console.log(Id);
    if(confirm('Are you sure to delete??')){
      this.departmentService.deleteDepartments(Id).subscribe(res=>{
        this.refreshDepartment();
        this.openSnackBar(res, null);
      },
      error => this.openSnackBar("Somethig went wrong...", null)
      );
    }
    
  }

  AddDepartment(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "70%";
    this.dialog.open(AddDeptComponent, dialogConfig);
  }
}
