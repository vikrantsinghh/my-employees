import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmployeeService } from 'src/app/services/employee.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-emp',
  templateUrl: './add-emp.component.html',
  styleUrls: ['./add-emp.component.scss']
})
export class AddEmpComponent implements OnInit {

  public lstDepartments: Array<string> = [];
  constructor(private dialogBox: MatDialogRef<AddEmpComponent>,
    public empService: EmployeeService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.resetForm();
    this.dropDownDepartments();
  }

  resetForm(form?: NgForm){
  if(form!=null)
    form.resetForm();

  this.empService.formData = {
      employeeID : 0,
      employeeName : '',
      department:'',
      emailID:'',
      doj:null
    }
  }

  onClose(){
    this.dialogBox.close();
    this.empService.filter("Refresh now");
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
    duration: 5000,
  });
  }

  dropDownDepartments(){
    this.empService.getDepartments().subscribe(res=>{
      res.forEach(element =>{
        this.lstDepartments.push(element.departmentName);
      });
    });
  }

  onSubmit(form: NgForm){
    console.log(form.value);
    this.empService.addEmployee(form.value).subscribe(res =>{
    this.resetForm(form);
    this.openSnackBar(res, null);
    //alert(res);
    });

  }
}