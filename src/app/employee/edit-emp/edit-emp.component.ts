import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgForm } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-edit-emp',
  templateUrl: './edit-emp.component.html',
  styleUrls: ['./edit-emp.component.scss']
})
export class EditEmpComponent implements OnInit {

  public lstDepartments: Array<string> = [];

  constructor(private dialogBox: MatDialogRef<EditEmpComponent>,
    public empService: EmployeeService,
    private _snackBar: MatSnackBar) { }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 5000,
    });
  }
  ngOnInit(): void {
    this.dropDownDepartments();
  }

  dropDownDepartments(){
    this.empService.getDepartments().subscribe(res=>{
      res.forEach(element =>{
        this.lstDepartments.push(element.departmentName);
      });
    });
  }
  onSubmit(form: NgForm){
    this.empService.updateEmployee(form.value).subscribe(res =>{
      this.openSnackBar(res, null);
     //alert(res);
    });
  }

  onClose(){
    this.dialogBox.close();
    this.empService.filter("Refresh now");
  }
}
