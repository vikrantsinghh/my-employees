import { Component, OnInit, ViewChild } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort'
import { MatInput } from '@angular/material/input'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';

import { Employee } from '../../models/employee'
import { AddEmpComponent } from 'src/app/employee/add-emp/add-emp.component'
import { EmployeeService } from 'src/app/services/employee.service';
import { EditEmpComponent } from '../edit-emp/edit-emp.component';

@Component({
  selector: 'app-show-emp',
  templateUrl: './show-emp.component.html',
  styleUrls: ['./show-emp.component.scss']
})
export class ShowEmpComponent implements OnInit {

  constructor(private empService : EmployeeService,
    private dialog : MatDialog,
    private _snackBar: MatSnackBar) { 
      this.empService.listen().subscribe((m:any)=>{
        console.log(m);
        this.refreshEmployee();
      });
    }

    isLoading = true;
    noData = false;
    lstEmployee : MatTableDataSource<any>;
    displayedColumns : string[] = ['Options', 'employeeID', 'employeeName', 'department', 'emailID', 'doj'];
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
    ngOnInit(): void {
      this.refreshEmployee();
    }
  
    openSnackBar(message: string, action: string) {
      this._snackBar.open(message, action, {
        duration: 5000,
      });
    }
    
    refreshEmployee(){
      this.empService.getEmployees().subscribe(data => {
        // console.log(data);
        this.isLoading = false;
        this.lstEmployee = new MatTableDataSource(data);
        this.lstEmployee.sort = this.sort;
        this.lstEmployee.paginator = this.paginator;
        this.lstEmployee.paginator._intl.itemsPerPageLabel = "Employees per page:";
      },
      error => this.isLoading = false
      );
    }
  
    applyFilter(filter: string){
      this.lstEmployee.filter = filter.trim().toLocaleLowerCase();
      if(this.lstEmployee.filteredData.length == 0)
        this.noData = true;
      else
        this.noData = false;
    }
  
    onEdit(emp : Employee){
      console.log(emp);
      this.empService.formData = emp;
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = "70%";
      this.dialog.open(EditEmpComponent, dialogConfig);
    }
  
    onDelete(Id: number){
      console.log(Id);
      if(confirm('Are you sure to delete??')){
        this.empService.deleteEmployee(Id).subscribe(res=>{
          this.refreshEmployee();
          this.openSnackBar(res, null);
        },
        error => this.openSnackBar("Somethig went wrong...", null)
        );
      }
      
    }
  
    AddEmployee(){
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = "70%";
      this.dialog.open(AddEmpComponent, dialogConfig);
    }

}
